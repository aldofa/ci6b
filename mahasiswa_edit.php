<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php $this->load->view('header');?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        MAHASISWA
        <small>Manajemen Data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mahasiswa</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class=" col-sm-12">

        <div class="box box-primary">
<div class="box-header">
    <b>From Edit Data Mahasiswa</b>
</div>

<form action="<?php echo site_url('mahasiswa/update');?>" method="post" enctype="multipart/form-data">
<div class="box-body">
<table class="table">
     <tr>
     <td width="150">NIM</td>
     <td width="5">:</td>
     <td>
            <div class="col-sm-6">
            <input type="text" value="<?php echo $mhs->nim;?>" disabled class="form-control">
        </div>
            <input type="hidden" name="nim" required value="<?php echo $mhs->nim;?>">
     </td>
</tr>     
<tr>
     <td width="150">Nama Mahasiswa</td>
     <td width="5">:</td>
     <td>
        <div class="col-sm-6">
     	<input type="text" name="nama" required value="<?php echo $mhs->nama;?>" class="form-control">
        </div>
     </td>
</tr>	
<tr>
     <td width="150">Kelas</td>
     <td width="5">:</td>
     <td>
            <div class="col-sm-6">
            <select name="kelas" class="form-control">
                <?php
                $kls[1] = NULL;
                $kls[2] = NULL;
              
                if($mhs->kelas == "12.6A.30") { $kls[1] = "selected";}
                if($mhs->kelas == "12.6B.30") { $kls[2] = "selected";}
               

                ?>
               <option value="12.6A.30" <?php echo $kls[1];?> >12.6A.30</option>
               <option value="12.6B.30" <?php echo $kls[2];?> >12.6B.30</option>

          </select>
          </div>     
     </td>
</tr>
<tr>
     <td width="150">Foto Mahasiswa</td>
     <td width="5">:</td>
     <td>
        <div class="col-sm-6">
        <input type="file" name="foto" class="form-control">
        <label>*Kosongkan jika tidak mengubah foto</label>
        </div>
     </td>
</tr>        
<tr>
     <td width="150"></td>
     <td width="5">:</td>
     <td>
        <div class="col-sm-6">
        <input type="submit" name="btn" value="Simpan Data" class="btn btn-primary">
        <a href="<?php echo site_url('mahasiswa');?>" class="btn btn-warning">
            Kembali
        </a> 
        </div>   
    </td>
  </tr> 
</table>
</div>
<div class="box-footer">
<?php
echo "User: ".$this->session->userdata('username');
?>
</div>
</form>

</div>

</div>
</div>
</section>
</div>

<?php $this->load->view('footer');?>